﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servidores
{
    class Program
    {
        static void Main(string[] args)
        {
            // se inicializa el servidor en ambiente local con el puerto 4404
            Servidor servidor = new Servidor("localhost",4404);
            servidor.Start();
            Console.ReadKey();
        }
    }
}
