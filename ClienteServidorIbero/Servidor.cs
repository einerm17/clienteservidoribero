﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;


namespace Servidores
{
    class Servidor
    {
        IPHostEntry host;
        IPAddress ipAddr;
        IPEndPoint endPoint;

        Socket s_Server;
        Socket s_Client;

        /// <summary>
        /// Constructor recibe la dirección ip y el puerto porque se inicializa el servidor
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public Servidor(string ip, int port)
        {
            host = Dns.GetHostByName(ip);
            ipAddr = host.AddressList[0];
            endPoint = new IPEndPoint(ipAddr, port);

            // Se inicializa el socket para realizar comunicación
            s_Server = new Socket(ipAddr.AddressFamily,SocketType.Stream,ProtocolType.Tcp);
            s_Server.Bind(endPoint);
            s_Server.Listen(10);
        }

        /// <summary>
        /// Función para iniciar el servidor
        /// </summary>
        public void Start()
        {
            Console.WriteLine("Servidor iniciado");

            byte[] buffer;
            string message;

            Thread t;

            // Ciclo infinito para esperar conexiones de clientes
            while (true)
            {
                Console.WriteLine("Esperando conexiones.....");
                s_Client = s_Server.Accept();

                // Se inicia un hilo para permitir la conexiones de diferentes clientes
                t = new Thread(clienteConexion);
                t.Start(s_Client); 
                Console.WriteLine("Se ha conectado un cliente");
            }
            
        }

        /// <summary>
        /// Función conexion cliente recibe un objeto
        /// </summary>
        /// <param name="s"></param>
        public void clienteConexion(object s)
        {
            Socket s_Client = (Socket)s;
            byte[] buffer;
            string message;

            while (true)
            {
                // se inicializa un buffer que va a contener los datos enviados desde el cliente
                buffer = new byte[1024];
                s_Client.Receive(buffer);
                // Se convierte el buffer que es un arreglo de bytes a un string para poder mostrarlo en consola
                message = Encoding.ASCII.GetString(buffer);

                message = message.Replace("\0", "");

                // Se muestra el mensaje enviado desde el cliente en la consola
                Console.WriteLine("Se recibio el mensaje: " + message);
                Console.Out.Flush();
            }
        }

    }
}
