﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Cliente
{
    class Cliente
    {
        IPHostEntry host;
        IPAddress ipAddr;
        IPEndPoint endPoint;

        Socket s_Client;

        /// <summary>
        /// Constructor recibe la dirección ip y el puerto porque se inicializa el cliente
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public Cliente(string ip, int port)
        {
            host = Dns.GetHostByName(ip);
            ipAddr = host.AddressList[0];
            endPoint = new IPEndPoint(ipAddr, port);

            // Se inicializa el socket para realizar comunicación
            s_Client = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
        }

        /// <summary>
        /// Función para iniciar el cliente
        /// </summary>
        public void Start()
        {
            s_Client.Connect(endPoint);
        }

        /// <summary>
        /// Función encargada de recibir el mensaje, ingresado por el cliente
        /// </summary>
        /// <param name="msg"></param>
        public void Send(string msg)
        {
            // Convierte el mensaje en un arreglo de bytes
            byte[] byteMsg = Encoding.ASCII.GetBytes(msg);

            // Envia el mensaje al servidor
            s_Client.Send(byteMsg);
            Console.WriteLine("Mensaje Enviado");
        }

    }
}
