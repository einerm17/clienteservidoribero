﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cliente
{
    class Program
    {
        static void Main(string[] args)
        {
            // Se inicializa el cliente en ambiente local con el puerto 4404, debe ser el mismo que tiene el servidor
            Cliente cliente = new Cliente("localhost",4404);
            cliente.Start();
            string texto;

            while (true)
            {
                // Se espera mensaje que introduzca el cliente, mediante teclado
                Console.WriteLine("Introduzca el texto que quiere enviar: ");
                texto = Console.ReadLine();

                // El texto ingresado por el cliente es enviado a función que envia el mensaje al servidor
                cliente.Send(texto);
            }
            
        }
    }
}
